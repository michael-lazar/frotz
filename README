FROTZ V2.45pre - An interpreter for all Infocom and other Z-machine games.
Complies with standard 1.0 of Graham Nelson's specification.

Originally written by Stefan Jokisch in 1995-1997.
Ported to Unix by Galen Hazelwood.
Reference code and Unix port currently maintained by David Griffith.

- Compiles and runs on most common flavors of Unix, both open source and not.
- Plays all Z-code games including V6.
- Sound support through libao.
- Config files.
- Configurable error checking.
- Distributed under the GNU Public License.


For information on what Interactive Fiction is and how to play it, see the
file "HOW_TO_PLAY".

For installation information, see the file "INSTALL".

For update history, see the file "Changelog".

For information on known bugs in Frotz, see the file "BUGS".

The Frotz homepage is at https://661.org/proj/if/frotz/

The latest information on and source code of Unix Frotz is available at 
https://gitlab.com/DavidGriffith/frotz
The old repository at Github is now deprecated. It will serve as a 
mirror for the time being, but will eventually be deleted.

The latest release of Unix Frotz is available from the Interactive
Fiction Archive at:
http://www.ifarchive.org/if-archive/infocom/interpreters/frotz/
	frotz-<version>.tar.gz
and
ftp://ftp.ifarchive.org/if-archive/infocom/interpreters/frotz/
	frotz-<version>.tar.gz


The Interactive Fiction Archive has several mirrors which may be better
choices depending on where you live.

	USA: http://ifarchive.jmac.org
	USA: http://ifmirror.russotto.net
	USA: http://ifarchive.smallwhitehouse.org
	USA: http://ifarchive.info
	USA: http://ifarchive.ifreviews.org
	USA: ftp://ifarchive.ifreviews.org
	USA: http://mirrors.ibiblio.org/interactive-fiction/
	USA: gopher://gopher.661.org/1/if-archive
